import boto3
import sys
import argparse

def describe_ec2_instances(client, accum_accounts=[], token=None, max_results=10):
    """
    Pulling all the EC2 instances using pagination
    """
    if token:
        response = client.describe_instances(NextToken=token, MaxResults=max_results)
    else:
        response = client.describe_instances(MaxResults=max_results)

    accounts = response['Reservations']
    token = response.get('NextToken',None)
    current_accounts = []
    for account in accounts:
        current_accounts.append(account)
    if token:
        return describe_ec2_instances(client, accum_accounts + current_accounts, 
                                        token=token, max_results=max_results)
    
    return  accum_accounts + current_accounts


def print_tabular(resp):
    '''
    Prints Tabular Data
    '''
    for tag in resp['Instances'][0]['Tags']:
        if list(tag.values())[0] == 'Name':
            name = tag['Value']
    
    prip = resp['Instances'][0]['NetworkInterfaces'][0]['PrivateIpAddress']
    try:
        pubip = resp['Instances'][0]['NetworkInterfaces'][0]['Association']['PublicIp']
    except:
        pubip = ''
    print("{} {} {:14} {:14} {} ".format(resp['Instances'][0]['NetworkInterfaces'][0]['OwnerId'], resp['Instances'][0]['InstanceId'], prip, pubip, name))


def main():
    '''
    Uses argparse to get arguments from user and then calls other functions accordingly
    '''
    parser = argparse.ArgumentParser(description='Script to get specific information about an ec2 instance. If no argument is passed, all instasnces will be displayed')
    parser.add_argument('-s', '--search', nargs='+',  help='filter parameters with id=123 OR name=windows OR publicip=128.91.26.54')
    parser.add_argument('-p', '--profile',action='store', help='Name of AWS Profile')

    args = parser.parse_args()
    aws_prof = args.profile
    profile = boto3.session.Session(profile_name=aws_prof, region_name="us-east-1")
    ec2_client = profile.client('ec2')
    
    if len(sys.argv) <= 3:
        print("No search arguments provided. Current EC2 instances are:")
        for ac in describe_ec2_instances(ec2_client):
            print_tabular(ac) 
    else:
        search_list = []    
        for p in args.search:
            p = p.split('=')
            search_list.append({str(p[0]): str(p[1])})
        flag = list(search_list[0].keys())[0]
        if flag == 'id':
            for ac in describe_ec2_instances(ec2_client):
                if search_list[0]['id'] in ac['Instances'][0]['InstanceId']:
                    print_tabular(ac)
        elif flag == 'name':
            for ac in describe_ec2_instances(ec2_client):
                for tag in ac['Instances'][0]['Tags']:
                    if list(tag.values())[0] == 'Name':
                        if search_list[0]['name'].upper() in (tag['Value'].upper()):
                            print_tabular(ac)
        elif flag =='publicip':
            for ac in describe_ec2_instances(ec2_client):
                try:
                    if search_list[0]['publicip'] in ac['Instances'][0]['NetworkInterfaces'][0]['Association']['PublicIp']:
                        print_tabular(ac)
                except:
                    pass
        elif flag =='privateip':
            for ac in describe_ec2_instances(ec2_client):
                if search_list[0]['privateip'] in ac['Instances'][0]['NetworkInterfaces'][0]['PrivateIpAddress']:
                    print_tabular(ac)
                    
                    
if __name__ == '__main__':
    main()
